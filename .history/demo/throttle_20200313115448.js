/**
 * 节流，节省资源，在某个时间段内允许触发
 * 使用场景 可能是一些动画，比方说检测某些元素的位置，监听位置变化，也比方说射击事件，一直按压键盘发射子弹，要保证
 * 比方说 1秒内 请求均匀请求5次 
 * 核心逻辑 setTimeout ,两次请求的时间间隔 不能小于设置的间隔值
 * @param {function} callback 
 * @param {number} threshhold 
 */
var before = 0; 
function throttle(callback, threshhold = 1000){
    now = new Date().getTime();
    callback();
    if(now - before < threshhold){ 
        return;
    }
    else{
        before = now;
        setTimeout(function(){
            callback();
        },threshhold)
    }
}