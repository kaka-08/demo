let now = 0;
let timer = null;


/**
 * 去抖, 顾名思义： 去除抖动。。。
 * 使用场景，用户多次点击按钮，只执行最后一次。 可以执行之后置灰操作按钮，提示交互
 * 核心逻辑 setTimeout, 每次点击按钮，清除定时器，重新开始及时  
 * @param { func } callback, the function that need to execute
 * @param { number } wait , default 300ms
 */
 function debounce(callback,wait = 300){
    clearTimeout(timer); //优先清除定时器
    timer = setTimeout(()=>{
        callback();
    }, wait)
}


 