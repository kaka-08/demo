var now = 0;

/**
 * 去抖, 顾名思义： 去除抖动。。。
 * 使用场景，用户多次点击按钮，只执行最后一次。 可以执行之后置灰操作按钮，提示交互
 * 核心逻辑 setTimeout,  
 * @param { func } callback, the function that need to execute
 * @param { number } wait , default 300ms
 */
 function debounce(callback,wait = 1000){
    let timer = null;
    console.log(new Date().getTime() - now);
    if(new Date().getTime() - now < wait){
        now = new Date().getTime();
        clearTimeout(timer);
    }else{
        timer = setTimeout(()=>{
            callback();
        }, 300)
    };
}


// 第一次点击  设置时间 

// 紧接着第二次点击  计算这两次时间差

 