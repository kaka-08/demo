var before = 0;

/**
 * 去抖, 顾名思义： 去除抖动。。。
 * 使用场景，用户多次点击按钮，只执行最后一次。 可以执行之后置灰操作按钮，提示交互
 * 核心逻辑 setTimeout,  
 * @param { func } callback, the function that need to execute
 * @param { number } wait , default 300ms
 */
 function debounce(callback,wait = 1000){
    let timer = null;
    now = new Date().getTime(); //当前程序执行的时间
    console.log('now - before %o', now - before);
    if(now - before < wait){
        clearTimeout(timer);
    };
    timer = setTimeout(()=>{
        callback();
        before =  new Date().getTime();
    }, 300)
 }

 