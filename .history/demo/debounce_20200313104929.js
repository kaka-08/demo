var before = 0;

/**
 * 去抖, 顾名思义： 去除抖动。。。
 * 使用场景，用户多次点击按钮，只执行最后一次。 可以执行之后置灰操作按钮，提示交互
 * 核心逻辑 setTimeout,  
 * @param { func } callback, the function that need to execute
 * @param { number } wait , default 300ms
 */
 function debounce(callback,wait = 300){
    now = new Date().getTime; //当前程序执行的时间
    if(now - before < wait){
        return
    };
    return setTimeout(()=>{
        callback();
        before =  new Date().getTime();
    }, 300)
 }

 